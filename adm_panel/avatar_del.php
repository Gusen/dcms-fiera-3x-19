<?
include_once '../sys/inc/start.php';
include_once '../sys/inc/compress.php';
include_once '../sys/inc/sess.php';
include_once '../sys/inc/home.php';
include_once '../sys/inc/settings.php';
include_once '../sys/inc/db_connect.php';
include_once '../sys/inc/ipua.php';
include_once '../sys/inc/fnc.php';
include_once '../sys/inc/adm_check.php';
include_once '../sys/inc/user.php';
user_access('ava_dell',null,'/');
adm_check();


if (isset($_GET['id'])) $ank['id'] = intval($_GET['id']);


if (mysql_result(mysql_query("SELECT COUNT(*) FROM `user` WHERE `id` = '{$ank['id']}' LIMIT 1"),0) == 0)
exit(header("Location: /"));

$ank = get_user($ank['id']);

if ($user['group_access'] < $ank['group_access'] and user_access('dell_avatar'))
{
	$_SESSION['message'] = lang('Не хватает прав');
	exit(header("Location: /"));
}

if ($user['id'] == $ank['id'])
{
	$_SESSION['message'] = lang('Свои аватары удалять нельзя');
	exit(header("Location: /"));
}

if (!is_file(H.'files/avatars/'.$ank['id'].'.png'))
{
	$_SESSION['message'] = lang('У пользователя не установлен аватар');
	exit(header("Location: /"));
}


$set['title']= lang('Удаление аватара');
include_once '../sys/inc/thead.php';
title();
aut();

//блокировка статуса
if (isset($_POST['delete_avatar']))
{

$msg = my_esc($_POST['prich']);

if (strlen2($msg) < 3)
{
	$_SESSION['message'] = lang('Нужно указать причину подробнее ');
	exit(header("Location: ".APANEL."/avatar_del.php?id=".$ank['id']));
}

if (isset($_POST['avtor']))
$av = ($_POST['avtor'] == 1 ? ' 
Модератор [url=/'. $user['mylink'] .']'. $user['nick'] .'[/url]':false);
else
$av = null;


$text = 'Ваш Аватар Удален!
Причина удаления : 
'. $msg . $av;

mail_send(0,$ank['id'],$text);
$_SESSION['message'] = lang('Автар Удален');


	@unlink(H."files/avatars/{$ank['id']}.png");
	$_SESSION['message'] = lang('Аватар удален');
	exit(header("Location: /id".$ank['id']));
}

echo "<center><div class='p_m'>";
avatar($ank['id'],200,200);
echo "</div></center>";

echo "<div class='p_m'><form action='' method='post'>";
echo lang('Причина')." <textarea name='prich' class='form_a'></textarea><br/>";
echo "<label><input type='checkbox' name='avtor' value='1' /> ".lang('Указать кто удалил')."</label><br/>";
echo "<input class='form_a_bottom'  name='delete_avatar' type='submit' value='".lang('Удалить Аватар')."' /> 
<a href='/{$ank['mylink']}'>".lang('Отмена')."</a>";
echo "</form></div>";

echo "<div class='foot'>";
echo "&laquo;<a href='".APANEL."/'>В админку</a><br />";
echo "</div>";

include_once '../sys/inc/tfoot.php';
?>